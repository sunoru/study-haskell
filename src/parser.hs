module Main where
import Control.Monad
import Data.Array
import Data.Complex
import Data.Ratio
import Numeric
import System.Environment
import Text.ParserCombinators.Parsec hiding (spaces)

main :: IO()
main = do line <- getLine
          (putStrLn . show . eval . readExpr) line

symbol :: Parser Char
symbol = oneOf "!$%&|*+-/:<=?>@^_~"

readExpr :: String -> LispVal
readExpr input = case parse parseExpr "lisp" input of
    Left err -> String $ "No match: " ++ show err
    Right val -> val

spaces :: Parser ()
spaces = skipMany1 space

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | IntNumber Integer
             | String String
             | Bool Bool
             | Character Char
             | Float Double
             | Ratio Rational
             | Complex (Complex Double)
             | Vector (Array Int LispVal)

escapedChars :: Parser String
escapedChars = do char '\\'
                  x <- oneOf "\\\"ntr"
                  return $ case x of
                             '\\' -> [x]
                             '\"' -> [x]
                             'n'  -> "\n"
                             't'  -> "\t"
                             'r'  -> "\r"
 

parseString :: Parser LispVal
parseString = do char '"'
                 x <- many $ many1 (noneOf "\"\\") <|> escapedChars
                 char '"'
                 return $ String (concat x)

parseAtom :: Parser LispVal
parseAtom = do first <- letter <|> symbol
               rest <- many (letter <|> digit <|> symbol)
               let atom = [first] ++ rest
               return $ Atom atom

parseBool :: Parser LispVal
parseBool = do char '#'
               x <- oneOf "tf"
               return $ case x of
                          't' -> Bool True
                          'f' -> Bool False

parseBases :: Parser LispVal
parseBases = do char '#'
                b <- oneOf "bodx"
                case b of
                  -- TODO 'b' -> 
                  'o' -> do x <- many1 octDigit
                            return $ IntNumber (fst $ readOct x !! 0)
                  'd' -> many1 digit >>= return . IntNumber . read
                  'x' -> do x <- many1 hexDigit
                            return $ IntNumber (fst $ readHex x !! 0)

parseIntNumber :: Parser LispVal
parseIntNumber = parseBases <|> (many1 digit >>= return . IntNumber . read)

parseCharacter :: Parser LispVal
parseCharacter = do string "#\\"
                    x <- try (string "space" <|> string "newline") <|>
                         do p <- anyChar
                            notFollowedBy alphaNum
                            return [p]
                    (return . Character) $ case x of
                        "space" -> ' '
                        "newline" -> '\n'
                        otherwise -> x !! 0            

parseFloat :: Parser LispVal
parseFloat = do a <- many1 digit
                char '.'
                b <- many1 digit
                return . Float $ fst ((readFloat (a++"."++b)) !! 0)

parseRatio :: Parser LispVal
parseRatio = do a <- many1 digit
                char '/'
                b <- many1 digit
                return $ Ratio ((read a) % (read b))

toDouble :: LispVal -> Double
toDouble(Float f) = realToFrac f
toDouble(IntNumber n) = fromIntegral n

parseComplex :: Parser LispVal
parseComplex = do a <- (try parseFloat <|> parseIntNumber)
    -- there's a bug that #xf+10i will be 15.0+10.0i rather than 15.0+16.0i
                  char '+'
                  b <- (try parseFloat <|> parseIntNumber)
                  char 'i'
                  return $ Complex (toDouble a :+ toDouble b)

parseExpr :: Parser LispVal
parseExpr = parseAtom <|> parseString
            <|> try parseRatio <|> try parseComplex <|> try parseFloat
            <|> try parseBool <|> try parseCharacter <|> try parseIntNumber
            <|> try (do string "#("
                        x <- parseVector
                        char ')'
                        return x)
            <|> parseQuoted
            <|> do char '('
                   x <- (try parseList) <|> parseDottedList
                   char ')'
                   return x
            <|> parseQuasiQuoted <|> parseUnQuote

parseList :: Parser LispVal
parseList = liftM List $ sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
    head <- endBy parseExpr spaces
    tail <- char '.' >> spaces >> parseExpr
    return $ DottedList head tail

parseQuoted :: Parser LispVal
parseQuoted = do
    char '\''
    x <- parseExpr
    return $ List [Atom "quote", x]

parseQuasiQuoted :: Parser LispVal
parseQuasiQuoted = do char '`'
                      x <- parseExpr
                      return $ List [Atom "quasiquote", x]

parseUnQuote :: Parser LispVal
parseUnQuote = do char ','
                  x <- parseExpr
                  return $ List [Atom "unquote", x]

parseVector :: Parser LispVal
parseVector = do arrayValues <- sepBy parseExpr spaces
                 return $ Vector (listArray (0, (length arrayValues -1)) arrayValues)

showVal :: LispVal -> String
showVal (String contents) = "\"" ++ contents ++ "\""
showVal (Atom name) = name
showVal (IntNumber contents) = show contents
showVal (Bool True) = "#t"
showVal (Bool False) = "#f"
showVal (Character char) = [char]
showVal (List contents) = "(" ++ unwordsList contents ++ ")"
showVal (DottedList head tail) = "(" ++ unwordsList head ++ " . " ++ showVal tail ++ ")"
showVal (Float contents) = show contents
showVal (Ratio contents) = show contents
showVal (Complex contents) = show contents
-- showVal (Vector contents) = "#(" ++ unwordsList contents ++ ")"

unwordsList :: [LispVal] -> String
unwordsList = unwords . map showVal

instance Show LispVal where show = showVal

eval :: LispVal -> LispVal
eval val@(String _) = val
eval val@(IntNumber _) = val
eval val@(Float _) = val
eval val@(Bool _) = val
eval val@(Ratio _) = val
eval val@(Complex _) = val
eval (List [Atom "quote", val]) = val
eval (List (Atom func : args)) = apply func $ map eval args

apply :: String -> [LispVal] -> LispVal
apply func args = maybe (Bool False) ($ args) $ lookup func primitives

primitives :: [(String, [LispVal] -> LispVal)]
primitives = [("+", numericBinop (+)),
              ("-", numericBinop (-)),
              ("*", numericBinop (*)),
              ("/", numericBinop div),
              ("mod", numericBinop mod),
              ("quotient", numericBinop quot),
              ("remainder", numericBinop rem),
              ("symbol?", unaryOp symbolp),
              ("string?", unaryOp stringp),
              ("integer?", unaryOp integerp),
              ("real?", unaryOp realp),
              ("rational?", unaryOp rationalp),
              ("complex?", unaryOp complexp),
              ("number?", unaryOp numberp),
              ("boolean?", unaryOp booleanp),
              ("list?", unaryOp listp)]

unaryOp :: (LispVal -> LispVal) -> [LispVal] -> LispVal
unaryOp op [param] = op param

symbolp :: LispVal -> LispVal
symbolp (Atom _) = Bool True
symbolp _ = Bool False
stringp :: LispVal -> LispVal
stringp (String _) = Bool True
stringp _ = Bool False
integerp :: LispVal -> LispVal
integerp (IntNumber _) = Bool True
integerp _ = Bool False
realp :: LispVal -> LispVal
realp (Float _) = Bool True
realp _ = Bool False
rationalp :: LispVal -> LispVal
rationalp (Ratio _) = Bool True
rationalp _ = Bool False
complexp :: LispVal -> LispVal
complexp (Complex _) = Bool True
complexp _ = Bool False
numberp :: LispVal -> LispVal
numberp _ = numericBinop (||) (integerp _) (realp _) (rationalp _) (complexp _)
booleanp :: LispVal -> LispVal
booleanp (Bool _) = Bool True
booleanp _ = Bool False
listp :: LispVal -> LispVal
listp (List _) = Bool True

boolBoolBinop :: (LispVal -> ThrowsError a) -> (a -> a -> Bool) -> [LispVal] -> ThrowsError LispVal
boolBoolBinop unpacker op args = if length args == 2
                                 -- then throwError $ Number 2 args
                                 do left <- unpacker $ args !! 0
                                    right <- unpacker $ args !! 1
                                    return $ Bool $ left op right
e l s e do l e f t <− u n p a c k e r $ a r g s ! ! 0
r i g h t <− u n p a c k e r $ a r g s ! ! 1
return $ Bool $ l e f t ‘ op ‘ r i g h t

numericBinop :: (Integer -> Integer -> Integer) -> [LispVal] -> LispVal
numericBinop op params = IntNumber $ foldl1 op $ map unpackNum params

unpackNum :: LispVal -> Integer
unpackNum (IntNumber n) = n
unpackNum (String n) = let parsed = reads n in
                          if null parsed
                            then 0
                            else fst $ parsed !! 0
unpackNum (List [n]) = unpackNum n
unpackNum _ = 0
